package com.mboss.auth.security.user;

import com.mboss.core.entity.user.ApplicationUser;
import com.mboss.core.repository.user.IUserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.Collection;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
@Slf4j
public class UserDetailServiceImpl implements UserDetailsService {

	private final IUserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) {
		log.info("Buscando no BD Usuario com nome '{}'", username);

		ApplicationUser user = userRepository.findByNome(username);
		log.info("Autenticação encontrou '{}'", user);

		if (user == null)
			throw new UsernameNotFoundException("Usuario não encontrado");

		return new CustomUserDetails(user);
	}

	private static final class CustomUserDetails extends ApplicationUser implements UserDetails {

		private static final long serialVersionUID = 1L;

		CustomUserDetails(@NotNull ApplicationUser user) {
			super(user);
		}

		@Override
		public Collection<? extends GrantedAuthority> getAuthorities() {
			return AuthorityUtils.commaSeparatedStringToAuthorityList(
					"ROLE_" + this.getRole());
		}

		@Override
		public String getPassword() {
			return this.getSenha();
		}

		@Override
		public String getUsername() {
			return this.getNome();
		}

		@Override
		public boolean isAccountNonExpired() {
			return true;
		}

		@Override
		public boolean isAccountNonLocked() {
			return true;
		}

		@Override
		public boolean isCredentialsNonExpired() {
			return true;
		}

		@Override
		public boolean isEnabled() {
			return true;
		}

	}

}
