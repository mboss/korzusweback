package com.mboss.auth.security.config;

import org.springframework.context.annotation.Configuration;

import com.mboss.core.docs.BaseSwaggerConfig;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig extends BaseSwaggerConfig {

	public SwaggerConfig() {
		super("com.mboss.auth.controller");
	}

}