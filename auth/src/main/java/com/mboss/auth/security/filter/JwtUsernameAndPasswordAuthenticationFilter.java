package com.mboss.auth.security.filter;

import java.io.IOException;
import java.util.Collections;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mboss.core.entity.user.ApplicationUser;
import com.mboss.core.property.JwtConfiguration;
import com.mboss.security.token.creater.TokenCreator;
import com.nimbusds.jwt.SignedJWT;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class JwtUsernameAndPasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private final AuthenticationManager authManeger;
	private final JwtConfiguration jwtConfinguration;
	private final TokenCreator tokenCreator;

	@Override
	@SneakyThrows
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
		ApplicationUser user = new ObjectMapper().readValue(request.getInputStream(), ApplicationUser.class);

		if (user == null)
			throw new UsernameNotFoundException("Usuário ou senha não pode ser nulo");

		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
				user.getNome(), user.getSenha(), Collections.emptyList());

		token.setDetails(user);

		return authManeger.authenticate(token);

	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication auth) throws IOException, ServletException {
		SignedJWT signed = tokenCreator.createSignedJWT(auth);

		String encryptToken = tokenCreator.encryptToken(signed);

		response.addHeader(
				"Access-Control-Expose-Headers",
				"XSRF-TOKEN, " + jwtConfinguration.getHeader().getName());

		response.addHeader(
				jwtConfinguration.getHeader().getName(),
				jwtConfinguration.getHeader().getPrefix() + encryptToken);

	}
}
