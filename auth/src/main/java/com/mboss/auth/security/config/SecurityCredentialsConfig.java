package com.mboss.auth.security.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.mboss.auth.security.filter.JwtUsernameAndPasswordAuthenticationFilter;
import com.mboss.core.property.JwtConfiguration;
import com.mboss.security.config.SecurityTokenConfig;
import com.mboss.security.filter.JwtTokenAuthorizationFilter;
import com.mboss.security.token.converter.TokenConverter;
import com.mboss.security.token.creater.TokenCreator;

@EnableWebSecurity
public class SecurityCredentialsConfig extends SecurityTokenConfig {

	private final UserDetailsService userDetailsService;
	private final TokenCreator tokenCreator;
	private final TokenConverter tokenConverter;

	public SecurityCredentialsConfig(
			JwtConfiguration jwtConfiguration,
			@Qualifier("userDetailServiceImpl") UserDetailsService userDetailsService,
			TokenCreator tokenCreator,
			TokenConverter tokenConverter) {

		super(jwtConfiguration);

		this.userDetailsService = userDetailsService;
		this.tokenCreator = tokenCreator;
		this.tokenConverter = tokenConverter;
	}

	@Override
	protected void configure(HttpSecurity http) {

		try {
			http
					.addFilter(
							new JwtUsernameAndPasswordAuthenticationFilter(
									authenticationManager(),
									jwtConfiguration, tokenCreator))
					.addFilterAfter(
							new JwtTokenAuthorizationFilter(
									jwtConfiguration, tokenConverter),
							UsernamePasswordAuthenticationFilter.class);

			super.configure(http);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) {
		try {
			auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}
