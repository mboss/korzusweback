package com.mboss.auth.contoller;

import java.security.Principal;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mboss.core.entity.user.ApplicationUser;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/user")
@Api(value = "Usuario ")
public class UserInfoController {

	@GetMapping("/info")
	@ApiOperation(value = "Will retrieve the information from the user available in the token", response = ApplicationUser.class)
	public ResponseEntity<ApplicationUser> getUserInfo(Principal principal) {
		ApplicationUser app = (ApplicationUser) ((UsernamePasswordAuthenticationToken) principal).getPrincipal();

		return new ResponseEntity<>(app, HttpStatus.OK);
	}

}
