package com.mboss.core.property;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Configuration
@ConfigurationProperties(prefix = "jwt.config")
@Inheritance(strategy = InheritanceType.JOINED)
@Getter
@Setter
@ToString
public class JwtConfiguration {

	private String loginUrl = "/login/**";

	@NestedConfigurationProperty
	private Header header = new Header();

	private int expiration = 3600;
	private String privateKey = "D8iG7P8EbBUFtZ9Bz3uXUcJ9gcGkKNik";
	private String type = "encrypted";

	@Getter
	@Setter
	public static class Header {
		private String name = "Authorization";
		private String prefix = "Bearer ";
	}

}
