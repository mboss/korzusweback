package com.mboss.core.repository.pessoa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.mboss.core.entity.pessoa.PessoaCriacao;

@Transactional(readOnly = true)
public interface IPessoaCriacaoRepository extends JpaRepository<PessoaCriacao, Long> {

}
