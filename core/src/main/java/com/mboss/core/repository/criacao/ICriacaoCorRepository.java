package com.mboss.core.repository.criacao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mboss.core.entity.criacao.CriacaoCor;

public interface ICriacaoCorRepository extends JpaRepository<CriacaoCor, Long> {

	public CriacaoCor findByDescricao(String descricao);

}
