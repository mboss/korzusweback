package com.mboss.core.repository.criacao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mboss.core.entity.criacao.CriacaoRaca;

public interface ICriacaoRacaRepository extends JpaRepository<CriacaoRaca, Long> {

	public CriacaoRaca findByDescricao(String descricao);

}
