package com.mboss.core.repository.criacao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mboss.core.entity.criacao.CriacaoEspecie;

public interface ICriacaoEspecieRepository extends JpaRepository<CriacaoEspecie, Long> {

	public CriacaoEspecie findByDescricao(String descricao);

}
