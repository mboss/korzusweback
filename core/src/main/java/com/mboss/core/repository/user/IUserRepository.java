package com.mboss.core.repository.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.mboss.core.entity.user.ApplicationUser;

@Transactional(readOnly = true)
public interface IUserRepository extends JpaRepository<ApplicationUser, Long> {

	ApplicationUser findByNome(String nome);
}
