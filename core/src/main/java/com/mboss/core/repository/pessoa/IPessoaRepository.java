package com.mboss.core.repository.pessoa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.mboss.core.entity.pessoa.Pessoa;

@Transactional(readOnly = true)
public interface IPessoaRepository extends JpaRepository<Pessoa, Long> {

	public Pessoa findByNomerazao(String nome);

}
