package com.mboss.core.repository.criacao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mboss.core.entity.criacao.Criacao;

public interface ICriacaoRepository extends JpaRepository<Criacao, Long> {

	public Criacao findByNome(String nome);

}
