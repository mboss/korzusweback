package com.mboss.core.repository.pessoa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.mboss.core.entity.pessoa.PessoaMedico;

@Transactional(readOnly = true)
public interface IPessoaMedicoRepository extends JpaRepository<PessoaMedico, Long> {

}
