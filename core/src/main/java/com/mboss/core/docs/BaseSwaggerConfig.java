package com.mboss.core.docs;

import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

public class BaseSwaggerConfig {

	private final String basePackage;

	public BaseSwaggerConfig(String basePackage) {
		this.basePackage = basePackage;
	}

	@Bean
	public Docket productApi() {
		return new Docket(
				DocumentationType.SWAGGER_2)
						.select()
						.apis(RequestHandlerSelectors.basePackage(basePackage))
						.build()
						.apiInfo(metaData());

	}

	private ApiInfo metaData() {
		return new ApiInfoBuilder()
				.title("Korzus WEB")
				.description("\"Endpoitns disponibilizadas\"")
				.version("1.0.0")
				.license("MBoss")
				.licenseUrl("https://www.mboss.com.br")
				.build();
	}
}