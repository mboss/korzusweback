package com.mboss.core.entity.criacao;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Criacao {

	@Id
	@NotBlank
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;

	@NotBlank(message = "Nome do animal é obrigatório.")
	private String nome;

	private String sexo;
	private String porte;
	private String idade;
	private String castrado;
	private String anotacao;

	@ApiModelProperty(required = false, example = "dd/MM/YYYY hh:mm")
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	private LocalDateTime datanascimento;

	private boolean pedigree;
	private boolean ativo;

	// @Lazy
	// @OneToOne
	// @ApiModelProperty(required = false, example = "Necessita ter a 'Espécie'
	// presente neste json cadastrada antes.")
	// private CriacaoEspecie criacaoEspecie;
	//
	// @org.hibernate.validator.internal.util.stereotypes.Lazy
	// @OneToOne
	// @ApiModelProperty(required = false, example = "Necessita ter a 'Raca'
	// presente neste json cadastrada antes.")
	// private CriacaoRaca criacaoRaca;
	//
	// @Lazy
	// @OneToOne
	// @ApiModelProperty(required = false, example = "Necessita ter a 'Cor' presente
	// neste json cadastrada antes.")
	// private CriacaoCor criacaoCor;

}
