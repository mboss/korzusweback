package com.mboss.core.entity.pessoa;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.validator.constraints.br.CNPJ;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.context.annotation.Lazy;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class PessoaComplemento {

	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@CPF(message = "Formato CPF inválido.")
	@Column(columnDefinition = "character (70)")
	private String cpf;

	@CNPJ(message = "Formato CNPJ inválido.")
	@Column(columnDefinition = "character (20)")
	private String cnpj;

	@Column(unique = true, columnDefinition = "character (70)")
	private String rg;

	@Column(columnDefinition = "character (20)")
	private String ie;

	@Email
	@Column(columnDefinition = "character (70)")
	private String email;

	@ApiModelProperty(required = false, example = "dd/MM/YYYY hh:mm")
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	private LocalDateTime datanascimento;

	private int idade;
	private String sexo;
	private String cor;
	private String estadocivil;
	private String naturalidade;
	private String nacionalidade;

	@Lazy
	@OneToOne
	@Cascade(value = { CascadeType.ALL })
	private PessoaParentesco pai;

	@Lazy
	@OneToOne
	@Cascade(value = { CascadeType.ALL })
	private PessoaParentesco mae;

	@Lazy
	@OneToOne
	@Cascade(value = { CascadeType.ALL })
	private PessoaParentesco conjuge;

	@Lazy
	@OneToMany
	@Cascade(value = { CascadeType.ALL })
	private List<PessoaEndereco> pessoaEndereco;

	private byte[] foto;

}
