package com.mboss.core.entity.pessoa;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.mboss.core.entity.criacao.Criacao;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@PrimaryKeyJoinColumn(name = "id")
public class PessoaCriacao extends Pessoa {

	private static final long serialVersionUID = -4161191647145895443L;

	@OneToMany
	@Cascade(value = { CascadeType.ALL })
	private List<Criacao> criacao;

}
