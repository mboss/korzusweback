package com.mboss.core.entity.criacao;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class CriacaoCor {

	// @Id
	// @GeneratedValue(strategy = GenerationType.IDENTITY)
	// @EqualsAndHashCode.Include
	// private Long id;
	//
	// @NotBlank(message = "The field 'title' is mandatory")
	// @Column(nullable = false)
	// private String title;

	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "criacao_cor")
	@SequenceGenerator(name = "criacao_cor", sequenceName = "cor_seq")
	private long id;

	@NotNull
	private String descricao;

	private boolean ativo;

}
