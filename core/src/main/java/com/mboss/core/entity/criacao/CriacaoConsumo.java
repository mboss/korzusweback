package com.mboss.core.entity.criacao;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class CriacaoConsumo {

	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "criacao_consumo")
	@SequenceGenerator(name = "criacao_consumo", sequenceName = "consumo_seq")
	private long id;

	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	private LocalDateTime datasistema = LocalDateTime.now();

	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	private LocalDateTime datavenda;

	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	private LocalDateTime dataaviso;

	// @OneToOne
	// private Produto produto;

	private String anotacao;
	private boolean status;

}
