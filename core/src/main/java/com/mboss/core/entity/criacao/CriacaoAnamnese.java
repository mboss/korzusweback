package com.mboss.core.entity.criacao;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mboss.core.entity.pessoa.PessoaMedico;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@PrimaryKeyJoinColumn(name = "id")
public class CriacaoAnamnese extends Criacao {

	@NotBlank(message = "O campo 'Descrição' é obrigatório")
	private String descricao;

	@ApiModelProperty(required = false, example = "dd/MM/YYYY hh:mm")
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	private LocalDateTime data;

	@OneToOne
	@ApiModelProperty(required = false, example = "Necessita ter a 'Médico' presente neste json cadastrada antes.")
	private PessoaMedico medico;

}
