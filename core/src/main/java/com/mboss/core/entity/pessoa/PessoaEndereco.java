package com.mboss.core.entity.pessoa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class PessoaEndereco {

	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(columnDefinition = "character (20)")
	private String pais;

	@Column(columnDefinition = "character (2)")
	private String estado;

	@Column(columnDefinition = "character (100)")
	private String cidade;
	private String bairro;

	@Column(columnDefinition = "character (10)")
	private String numeroresidencia;

	private String logradouro;

	private String complemento;

}
