package com.mboss.core.entity.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApplicationUser {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Include
	private long id;

	@NotBlank(message = "Nome é obrigatório.")
	@Column(nullable = false)
	private String nome;

	@NotBlank(message = "Senha é obrigatória.")
	@Column(nullable = false)
	@ToString.Exclude
	private String senha;

	@NotBlank(message = "Role é obrigatório.")
	@Column(nullable = false)
	@Builder.Default
	private String role = "USER";

	public ApplicationUser(@NotNull ApplicationUser user) {
		this.id = user.getId();
		this.nome = user.getNome();
		this.senha = user.getSenha();
		this.role = user.getRole();
	}

}
