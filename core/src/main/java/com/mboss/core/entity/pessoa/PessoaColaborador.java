package com.mboss.core.entity.pessoa;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@PrimaryKeyJoinColumn(name = "id")
public class PessoaColaborador extends Pessoa {

	private static final long serialVersionUID = 1186593403849317938L;

	@ApiModelProperty(required = true, example = "dd/MM/YYYY hh:mm")
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	@NotBlank(message = "Data de Adimição é um campo obrigatório.")
	private LocalDateTime dataadmissao;

	@ApiModelProperty(required = false, example = "dd/MM/YYYY hh:mm")
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	private LocalDateTime datasaida;

	@OneToMany
	@Cascade(value = { CascadeType.ALL })
	@NotBlank(message = "Fun��o � um campo obrigat�rio.")
	@ApiModelProperty(required = true, example = ""
			+ "{Gar�om, Entragador, Usa Mobile}, "
			+ "{Professor, Secretario }, "
			+ "{Vendedor, Vendedor comicionado }")
	private List<PessoaFuncao> funcao;

	private int cargahoraria;

	private String primeirohorario;
	private String segundohorario;

	private double salario;
	private double comissao;
	private double descontomaximo;
}
