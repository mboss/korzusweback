package com.mboss.core.entity.pessoa;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter

@PrimaryKeyJoinColumn(name = "id")
public class PessoaFuncao extends Pessoa {

	private static final long serialVersionUID = 1L;

	@NotBlank(message = "Descrição é um campo obrigatório.")
	private String descricao;
	
	private boolean ativo;

}
