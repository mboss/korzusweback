package com.mboss.core.entity.pessoa;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.springframework.context.annotation.Lazy;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Getter
@Setter
public class Pessoa implements Serializable {

	private static final long serialVersionUID = 6961057815205671767L;

	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotBlank(message = "Nome raz�o � obrigat�rio.")
	private String nomerazao;
	private String nomefantasia;

	@ApiModelProperty(required = false, example = "dd/MM/YYYY hh:mm")
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	private LocalDateTime datacadastro = LocalDateTime.now();

	@Lazy
	@OneToOne
	@Cascade(value = { CascadeType.ALL })
	private PessoaComplemento pessoaComplemento;

	private boolean ativo;

}
