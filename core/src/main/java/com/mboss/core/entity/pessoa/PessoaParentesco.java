package com.mboss.core.entity.pessoa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CPF;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class PessoaParentesco {

	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotBlank(message = "Nome é obrigatorio.")
	private String nome;

	@CPF
	@Column(columnDefinition = "character (20)")
	private String cpf;

	@Column(columnDefinition = "character (20)")
	private String rg;

	@Email
	@Column(columnDefinition = "character (70)")
	private String email;

}
