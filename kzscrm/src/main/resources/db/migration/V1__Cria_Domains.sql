CREATE DOMAIN "D_ATIVO"
    AS character(1)
    DEFAULT 'S'
    NOT NULL;


CREATE DOMAIN "D_NOME"
  AS character varying(50);

CREATE DOMAIN "D_NOME100"
  AS character varying(100);

CREATE DOMAIN "D_NAOSIM"
  AS character(1)
  DEFAULT 'N'
  NOT NULL;

CREATE DOMAIN "D_SIMNAO"
  AS character(1)
  DEFAULT 'S'
  NOT NULL;