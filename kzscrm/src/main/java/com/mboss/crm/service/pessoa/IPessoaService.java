package com.mboss.crm.service.pessoa;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.http.ResponseEntity;

import com.mboss.core.entity.pessoa.Pessoa;

public interface IPessoaService {

	ResponseEntity<Boolean> deleteById(Long id);

	Optional<Pessoa> findById(Long id);

	Optional<ArrayList<Pessoa>> findAll();

	ResponseEntity<Pessoa> cadastrar(Pessoa pessoa);

	ResponseEntity<Pessoa> atualizar(Long id_pessoa, boolean status);

}
