package com.mboss.crm.service.criacao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.mboss.core.entity.criacao.CriacaoEspecie;
import com.mboss.core.repository.criacao.ICriacaoEspecieRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CriacaoEspecieServiceImpl implements ICriacaoEspecieService {

	private final ICriacaoEspecieRepository criacaoRepository;

	@Override
	public ResponseEntity<CriacaoEspecie> cadastrar(CriacaoEspecie criacaoEspecie) {
		return ResponseEntity.ok(criacaoRepository.save(criacaoEspecie));
	}

	@Override
	public ResponseEntity<CriacaoEspecie> atualizar(CriacaoEspecie criacaoEspecie) {
		return ResponseEntity.ok(criacaoRepository.save(criacaoEspecie));
	}

	@Override
	public Optional<List<CriacaoEspecie>> findAll() {
		return Optional.ofNullable(criacaoRepository.findAll());
	}

	@Override
	public Optional<CriacaoEspecie> findById(Long id) {
		return criacaoRepository.findById(id);
	}

	@Override
	public ResponseEntity<Boolean> deleteById(Long id) {
		criacaoRepository.deleteById(id);
		return ResponseEntity.ok(findById(id).isEmpty());
	}

	@Override
	public Optional<CriacaoEspecie> findByDescricao(String descricao) {
		return Optional.ofNullable(criacaoRepository.findByDescricao(descricao));
	}

}
