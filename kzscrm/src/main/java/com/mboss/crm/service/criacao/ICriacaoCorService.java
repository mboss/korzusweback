package com.mboss.crm.service.criacao;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;

import com.mboss.core.entity.criacao.CriacaoCor;

public interface ICriacaoCorService {

	ResponseEntity<CriacaoCor> cadastrar(CriacaoCor criacaoCor);

	ResponseEntity<CriacaoCor> atualizar(CriacaoCor criacaoCsor);

	Optional<List<CriacaoCor>> findAll();

	Optional<CriacaoCor> findById(Long id);

	ResponseEntity<Boolean> deleteById(Long id);

}
