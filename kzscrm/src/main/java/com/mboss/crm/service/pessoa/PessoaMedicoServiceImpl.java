package com.mboss.crm.service.pessoa;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.mboss.core.entity.pessoa.PessoaMedico;
import com.mboss.core.repository.pessoa.IPessoaMedicoRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PessoaMedicoServiceImpl implements IPessoaMedicoService {

	private final IPessoaMedicoRepository medicoRepository;

	@Override
	public Optional<List<PessoaMedico>> findAll() {
		List<PessoaMedico> list = medicoRepository.findAll();
		return Optional.of(list);
	}

	@Override
	public ResponseEntity<PessoaMedico> cadastrar(PessoaMedico medico) {
		return ResponseEntity.ok(medicoRepository.save(medico));
	}

	@Override
	public ResponseEntity<PessoaMedico> atualizar(Long id_medico, boolean status) {

		Optional<PessoaMedico> medico = findById(id_medico);

		if (medico.isPresent()) {
			medico.get().setAtivo(status);
			return ResponseEntity.ok(medicoRepository.save(medico.get()));
		}

		return new ResponseEntity<PessoaMedico>(HttpStatus.NO_CONTENT);

	}

	@Override
	public Optional<PessoaMedico> findById(Long id) {
		return medicoRepository.findById(id);
	}

	@Override
	public boolean deleteById(Long id) {
		medicoRepository.deleteById(id);
		return !findById(id).isPresent();
	}

}
