package com.mboss.crm.service.pessoa;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;

import com.mboss.core.entity.pessoa.PessoaCriacao;

public interface IPessoaCriacaoService {

	boolean deleteById(Long id);

	Optional<PessoaCriacao> findById(Long id);

	Optional<List<PessoaCriacao>> findAll();

	ResponseEntity<PessoaCriacao> cadastrar(PessoaCriacao pessoaCricao);

	ResponseEntity<PessoaCriacao> atualizar(Long id, boolean status);

}
