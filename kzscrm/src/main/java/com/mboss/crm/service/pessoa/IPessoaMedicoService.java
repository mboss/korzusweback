package com.mboss.crm.service.pessoa;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;

import com.mboss.core.entity.pessoa.PessoaMedico;

public interface IPessoaMedicoService {

	boolean deleteById(Long id);

	Optional<List<PessoaMedico>> findAll();

	Optional<PessoaMedico> findById(Long id);

	ResponseEntity<PessoaMedico> atualizar(Long id_medico, boolean status);

	ResponseEntity<PessoaMedico> cadastrar(PessoaMedico medico);

}
