package com.mboss.crm.service.criacao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.mboss.core.entity.criacao.CriacaoCor;
import com.mboss.core.repository.criacao.ICriacaoCorRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CriacaoCorServiceImpl implements ICriacaoCorService {

	private final ICriacaoCorRepository criacaoCorRepository;

	public ResponseEntity<CriacaoCor> cadastrar(CriacaoCor criacaoCor) {
		return ResponseEntity.ok(criacaoCorRepository.save(criacaoCor));
	}

	public ResponseEntity<CriacaoCor> atualizar(CriacaoCor criacaoCor) {
		return ResponseEntity.ok(criacaoCorRepository.save(criacaoCor));
	}

	public Optional<List<CriacaoCor>> findAll() {
		return Optional.ofNullable(criacaoCorRepository.findAll());

	}

	public Optional<CriacaoCor> findById(Long id) {
		return criacaoCorRepository.findById(id);
	}

	@Override
	public ResponseEntity<Boolean> deleteById(Long id) {
		criacaoCorRepository.deleteById(id);
		return ResponseEntity.ok(findById(id).isEmpty());
	}

	public Optional<CriacaoCor> findByDescricao(String descricao) {
		return Optional.ofNullable(criacaoCorRepository.findByDescricao(descricao));
	}

}
