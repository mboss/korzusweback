package com.mboss.crm.service.criacao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.mboss.core.entity.criacao.CriacaoRaca;
import com.mboss.core.repository.criacao.ICriacaoRacaRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CriacaoRacaServiceImpl implements ICriacaoRacaService {

	private final ICriacaoRacaRepository criacaoRepository;

	@Override
	public ResponseEntity<CriacaoRaca> cadastrar(CriacaoRaca criacao) {
		return ResponseEntity.ok(criacaoRepository.save(criacao));
	}

	@Override
	public ResponseEntity<CriacaoRaca> atualizar(CriacaoRaca criacao) {
		return ResponseEntity.ok(criacaoRepository.save(criacao));
	}

	@Override
	public Optional<List<CriacaoRaca>> findAll() {
		return Optional.ofNullable(criacaoRepository.findAll());
	}

	@Override
	public Optional<CriacaoRaca> findById(Long id) {
		return criacaoRepository.findById(id);
	}

	@Override
	public ResponseEntity<Boolean> deleteById(Long id) {
		criacaoRepository.deleteById(id);
		return ResponseEntity.ok(findById(id).isEmpty());
	}

	public Optional<CriacaoRaca> findByDescricao(String descricao) {
		return null;
	}

}
