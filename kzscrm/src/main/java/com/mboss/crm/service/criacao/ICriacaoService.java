package com.mboss.crm.service.criacao;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;

import com.mboss.core.entity.criacao.Criacao;

public interface ICriacaoService {

	boolean deleteById(Long id);

	Optional<Criacao> findById(Long id);

	Optional<List<Criacao>> findAll();

	ResponseEntity<Criacao> atualizar(Criacao criacao);

	ResponseEntity<Criacao> cadastrar(Criacao criacao);

}
