package com.mboss.crm.service.criacao;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;

import com.mboss.core.entity.criacao.CriacaoRaca;

public interface ICriacaoRacaService {

	ResponseEntity<CriacaoRaca> cadastrar(CriacaoRaca criacao);

	ResponseEntity<CriacaoRaca> atualizar(CriacaoRaca criacao);

	Optional<List<CriacaoRaca>> findAll();

	Optional<CriacaoRaca> findById(Long id);

	ResponseEntity<Boolean> deleteById(Long id);

}
