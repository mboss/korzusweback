package com.mboss.crm.service.pessoa;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.mboss.core.entity.pessoa.Pessoa;
import com.mboss.core.repository.pessoa.IPessoaRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PessoaServiceImpl implements IPessoaService {

	private final IPessoaRepository pessoaRepository;

	@Override
	public ResponseEntity<Pessoa> cadastrar(Pessoa pessoa) {
		return ResponseEntity.ok(pessoaRepository.save(pessoa));
	}

	@Override
	public ResponseEntity<Pessoa> atualizar(Long id_pessoa, boolean status) {

		Optional<Pessoa> pessoa = findById(id_pessoa);

		if (pessoa.isPresent()) {
			pessoa.get().setAtivo(status);
			return ResponseEntity.ok(pessoaRepository.save(pessoa.get()));
		}

		return new ResponseEntity<Pessoa>(HttpStatus.NO_CONTENT);

	}

	@Override
	public Optional<ArrayList<Pessoa>> findAll() {
		return Optional.ofNullable((ArrayList<Pessoa>) pessoaRepository.findAll());
	}

	@Override
	public Optional<Pessoa> findById(Long id) {
		return pessoaRepository.findById(id);
	}

	@Override
	public ResponseEntity<Boolean> deleteById(Long id) {
		pessoaRepository.deleteById(id);
		return ResponseEntity.ok(findById(id).isEmpty());
	}

}
