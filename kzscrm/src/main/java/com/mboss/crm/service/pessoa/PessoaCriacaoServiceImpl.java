package com.mboss.crm.service.pessoa;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.mboss.core.entity.pessoa.PessoaCriacao;
import com.mboss.core.repository.pessoa.IPessoaCriacaoRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PessoaCriacaoServiceImpl implements IPessoaCriacaoService {

	private final IPessoaCriacaoRepository criacaoRepository;

	@Override
	public Optional<List<PessoaCriacao>> findAll() {
		List<PessoaCriacao> list = criacaoRepository.findAll();
		return Optional.of(list);
	}

	@Override
	public ResponseEntity<PessoaCriacao> cadastrar(PessoaCriacao medico) {
		return ResponseEntity.ok(criacaoRepository.save(medico));
	}

	@Override
	public ResponseEntity<PessoaCriacao> atualizar(Long id_medico, boolean status) {

		Optional<PessoaCriacao> medico = findById(id_medico);

		if (medico.isPresent()) {
			medico.get().setAtivo(status);
			return ResponseEntity.ok(criacaoRepository.save(medico.get()));
		}

		return new ResponseEntity<PessoaCriacao>(HttpStatus.NO_CONTENT);

	}

	@Override
	public Optional<PessoaCriacao> findById(Long id) {
		return criacaoRepository.findById(id);
	}

	@Override
	public boolean deleteById(Long id) {
		criacaoRepository.deleteById(id);
		return !findById(id).isPresent();
	}

}
