package com.mboss.crm.service.criacao;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;

import com.mboss.core.entity.criacao.CriacaoEspecie;

public interface ICriacaoEspecieService {

	ResponseEntity<CriacaoEspecie> cadastrar(CriacaoEspecie criacaoEspecie);

	ResponseEntity<CriacaoEspecie> atualizar(CriacaoEspecie criacaoEspecie);

	Optional<List<CriacaoEspecie>> findAll();

	Optional<CriacaoEspecie> findById(Long id);

	ResponseEntity<Boolean> deleteById(Long id);

	Optional<CriacaoEspecie> findByDescricao(String descricao);

}
