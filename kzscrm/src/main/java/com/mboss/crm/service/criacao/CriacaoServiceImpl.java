package com.mboss.crm.service.criacao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.mboss.core.entity.criacao.Criacao;
import com.mboss.core.repository.criacao.ICriacaoRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CriacaoServiceImpl {

	private final ICriacaoRepository criacaoRepository;

	public ResponseEntity<Criacao> cadastrar(Criacao criacao) {
		return ResponseEntity.ok(criacaoRepository.save(criacao));
	}

	public ResponseEntity<Criacao> atualizar(Criacao criacao) {
		return ResponseEntity.ok(criacaoRepository.save(criacao));
	}

	public Optional<List<Criacao>> findAll() {
		return Optional.ofNullable(criacaoRepository.findAll());
	}

	public Optional<Criacao> findById(Long id) {
		return criacaoRepository.findById(id);
	}

	public boolean deleteById(Long id) {
		criacaoRepository.deleteById(id);
		return !findById(id).isPresent();
	}

}
