package com.mboss.crm.controller.pessoa;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mboss.core.entity.pessoa.PessoaMedico;
import com.mboss.crm.service.pessoa.PessoaMedicoServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/pessoas/medicos")
@Api(value = "medicos")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PessoaMedicoController {

	private PessoaMedicoServiceImpl medicoService;

	@ApiOperation(value = "Busca todos registros de Medicos")
	@GetMapping
	public Optional<List<PessoaMedico>> getAll() {
		return medicoService.findAll();

	}

	@ApiOperation(value = "Cadastra/Atualiza PessoaMedico ? ( medico.id == null : medico.id > 0 ) ")
	@PostMapping
	public ResponseEntity<PessoaMedico> setCadastroAtualiza(@RequestBody @Validated PessoaMedico medico) {
		return medicoService.cadastrar(medico);
	}

	@ApiOperation(value = "Ativar Cadastro")
	@PutMapping(path = "/ativar")
	public ResponseEntity<PessoaMedico> ativar(@RequestBody @Validated Long id_medico) {
		return medicoService.atualizar(id_medico, true);
	}

	@ApiOperation(value = "Desativar Cadastro")
	@PutMapping(path = "/desativar")
	public ResponseEntity<PessoaMedico> desativar(@RequestBody @Validated Long idPessoaMedico) {
		return medicoService.atualizar(idPessoaMedico, false);
	}

	@ApiOperation(value = "Busca por id")
	@GetMapping(path = "/id")
	public Optional<PessoaMedico> findById(@RequestParam @Validated Long id) {
		var test = medicoService.findById(id);
		System.out.println(test.get().getNomefantasia());
		return test;
	}

	// @ApiOperation(value = "Busca por medico2")
	// @GetMapping(path = "/medico2")
	// public Optional<List<PessoaMedicoMedico>> findAllMedico2() {
	// return medicoService.findAll2();
	// }

}
