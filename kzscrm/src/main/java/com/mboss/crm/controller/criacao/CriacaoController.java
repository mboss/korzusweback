package com.mboss.crm.controller.criacao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mboss.core.entity.criacao.Criacao;
import com.mboss.crm.service.criacao.CriacaoServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/criacoes")
@Api(value = "criacoes")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CriacaoController {

	private CriacaoServiceImpl criacaoService;

	@ApiOperation(value = "Cadastra criação")
	@PostMapping
	public ResponseEntity<Criacao> cadastrar(@RequestBody @Validated Criacao criacao) {
		return criacaoService.cadastrar(criacao);
	}

	@ApiOperation(value = "Ativar Cadastro")
	@PutMapping(path = "/ativar")
	public ResponseEntity<Criacao> ativar(@RequestBody @Validated Criacao cricao) {
		cricao.setAtivo(true);
		return criacaoService.atualizar(cricao);
	}

	@ApiOperation(value = "Desativar Cadastro")
	@PutMapping(path = "/desativar")
	public ResponseEntity<Criacao> desativar(@RequestBody @Validated Criacao cricao) {
		cricao.setAtivo(false);
		return criacaoService.atualizar(cricao);
	}

	@ApiOperation(value = "Atualizar Cadastro")
	@PutMapping(path = "/atualizar")
	public ResponseEntity<Criacao> atualizar(@RequestBody @Validated Criacao cricao) {
		return criacaoService.atualizar(cricao);
	}

	@ApiOperation(value = "Busca todos registros")
	@GetMapping
	public Optional<List<Criacao>> getAll() {
		return criacaoService.findAll();
	}

	@ApiOperation(value = "Busca por id")
	@GetMapping(path = "/id")
	public Optional<Criacao> getById(@RequestParam @Validated Long id) {
		return criacaoService.findById(id);
	}

}
