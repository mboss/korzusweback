package com.mboss.crm.controller.pessoa;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mboss.core.entity.pessoa.PessoaCriacao;
import com.mboss.crm.service.pessoa.PessoaCriacaoServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/pessoas/criacao")
@Api(value = "criacao")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PessoaCriacaoController {

	private PessoaCriacaoServiceImpl criacaoService;

	@ApiOperation(value = "Busca todos registros de Medicos")
	@GetMapping
	public Optional<List<PessoaCriacao>> getAll() {
		return criacaoService.findAll();

	}

	@ApiOperation(value = "Cadastra/Atualiza PessoaCriacao ? ( criacao.id == null : criacao.id > 0 ) ")
	@PostMapping
	public ResponseEntity<PessoaCriacao> setCadastroAtualiza(@RequestBody @Validated PessoaCriacao criacao) {
		return criacaoService.cadastrar(criacao);
	}

	@ApiOperation(value = "Ativar Cadastro")
	@PutMapping(path = "/ativar")
	public ResponseEntity<PessoaCriacao> ativar(@RequestBody @Validated Long id_criacao) {
		return criacaoService.atualizar(id_criacao, true);
	}

	@ApiOperation(value = "Desativar Cadastro")
	@PutMapping(path = "/desativar")
	public ResponseEntity<PessoaCriacao> desativar(@RequestBody @Validated Long idPessoaCriacao) {
		return criacaoService.atualizar(idPessoaCriacao, false);
	}

	@ApiOperation(value = "Busca por id")
	@GetMapping(path = "/id")
	public Optional<PessoaCriacao> findById(@RequestParam @Validated Long id) {
		return criacaoService.findById(id);
	}

	// @ApiOperation(value = "Busca por criacao2")
	// @GetMapping(path = "/criacao2")
	// public Optional<List<PessoaCriacaoMedico>> findAllMedico2() {
	// return criacaoService.findAll2();
	// }

}
