package com.mboss.crm.controller.criacao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mboss.core.entity.criacao.CriacaoRaca;
import com.mboss.crm.service.criacao.CriacaoRacaServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/criacoes/racas")
@Api(value = "racas")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CriacaoRacaController {

	private CriacaoRacaServiceImpl criacaoService;

	@ApiOperation(value = "Cadastra raca para criação")
	@PostMapping
	public ResponseEntity<CriacaoRaca> cadastrar(@RequestBody @Validated CriacaoRaca criacao) {
		return criacaoService.cadastrar(criacao);
	}

	@ApiOperation(value = "Atualizar Cadastro")
	@PutMapping(path = "/atualizar")
	public ResponseEntity<CriacaoRaca> atualizar(@RequestBody @Validated CriacaoRaca cricao) {
		return criacaoService.atualizar(cricao);
	}

	@ApiOperation(value = "Busca todos registros")
	@GetMapping
	public Optional<List<CriacaoRaca>> getAll() {
		return criacaoService.findAll();
	}

	@ApiOperation(value = "Busca por id")
	@GetMapping(path = "/id")
	public Optional<CriacaoRaca> getById(@RequestParam @Validated Long id) {
		return criacaoService.findById(id);
	}

	@ApiOperation(value = "Busca por decricao")
	@GetMapping(path = "/descricao")
	public Optional<CriacaoRaca> getById(@RequestParam @Validated String descricao) {
		return criacaoService.findByDescricao(descricao);
	}

	@ApiOperation(value = "Exclusao por id")
	@DeleteMapping(path = "/id")
	public ResponseEntity<Boolean> deleteById(Long id) {
		return criacaoService.deleteById(id);
	}

}
