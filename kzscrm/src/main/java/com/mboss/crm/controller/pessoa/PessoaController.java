package com.mboss.crm.controller.pessoa;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mboss.core.entity.pessoa.Pessoa;
import com.mboss.crm.service.pessoa.PessoaServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/pessoas")
@Api(value = "Pessoas")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PessoaController {

	private final PessoaServiceImpl pessoaService;

	@GetMapping
	@ApiOperation(value = "TESTE", response = Pessoa[].class)
	public Optional<ArrayList<Pessoa>> getAll() {
		return pessoaService.findAll();
	}

	@ApiOperation(value = "Cadastra/Atualiza Pessoa ? ( pessoa.id == null : pessoa.id > 0 ) ")
	@PostMapping
	public ResponseEntity<Pessoa> setCadastroAtualiza(@RequestBody @Validated Pessoa pessoa) {
		return pessoaService.cadastrar(pessoa);
	}

	@ApiOperation(value = "Ativar Cadastro")
	@PutMapping(path = "/ativar")
	public ResponseEntity<Pessoa> ativar(@RequestBody @Validated Long id_pessoa) {
		return pessoaService.atualizar(id_pessoa, true);
	}

	@ApiOperation(value = "Desativar Cadastro")
	@PutMapping(path = "/desativar")
	public ResponseEntity<Pessoa> desativar(@RequestBody @Validated Long idPessoa) {
		return pessoaService.atualizar(idPessoa, false);
	}

	@ApiOperation(value = "Busca por id")
	@GetMapping(path = "/id")
	public Optional<Pessoa> findById(@RequestParam @Validated Long id) {
		return pessoaService.findById(id);
	}

	@ApiOperation(value = "Exclusão por id \n retorna 'true' se item deletado")
	@DeleteMapping(path = "id/")
	public ResponseEntity<Boolean> deleteById(@RequestParam @Validated Long id) {
		return pessoaService.deleteById(id);
	}

}
