package com.mboss.crm.controller.criacao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mboss.core.entity.criacao.CriacaoCor;
import com.mboss.crm.service.criacao.CriacaoCorServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/criacoes/cores")
@Api(value = "cores")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CriacaoCorController {

	private CriacaoCorServiceImpl criacaoService;

	@ApiOperation(value = "Cadastra cores para criação")
	@PostMapping
	public ResponseEntity<CriacaoCor> cadastrar(@RequestBody @Validated CriacaoCor criacaoCor) {
		return criacaoService.cadastrar(criacaoCor);
	}

	@ApiOperation(value = "Atualizar Cadastro")
	@PutMapping(path = "/atualizar")
	public ResponseEntity<CriacaoCor> atualizar(@RequestBody @Validated CriacaoCor cricaoCor) {
		return criacaoService.atualizar(cricaoCor);
	}

	@ApiOperation(value = "Busca todos registros")
	@GetMapping
	public Optional<List<CriacaoCor>> getAll() {
		return Optional.empty();
	}

	@ApiOperation(value = "Busca por id")
	@GetMapping(path = "/id")
	public Optional<CriacaoCor> getById(@RequestParam @Validated Long id) {
		return Optional.empty();
	}

	@ApiOperation(value = "Busca por descricao")
	@GetMapping(path = "/descricao")
	public Optional<CriacaoCor> getById(@RequestParam @Validated String descricao) {
		return criacaoService.findByDescricao(descricao);
	}

	// @ApiOperation(value = "Exclusao por id")
	// @DeleteMapping(path = "/id")
	// public boolean deleteById(Long id) {
	// return criacaoService.deleteById(id);
	// }

}
