package com.mboss.crm.controller.criacao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mboss.core.entity.criacao.CriacaoEspecie;
import com.mboss.crm.service.criacao.CriacaoEspecieServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/criacoes/especies")
@Api(value = "especies")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CriacaoEspecieController {

	private CriacaoEspecieServiceImpl especieService;

	@ApiOperation(value = "Cadastra Especie")
	@PostMapping
	public ResponseEntity<CriacaoEspecie> cadastrar(@RequestBody @Validated CriacaoEspecie criacaoEspecie) {
		return especieService.cadastrar(criacaoEspecie);
	}

	@ApiOperation(value = "Atualizar Cadastro")
	@PutMapping(path = "/atualizar")
	public ResponseEntity<CriacaoEspecie> atualizar(@RequestBody @Validated CriacaoEspecie criacaoEspecie) {
		return especieService.atualizar(criacaoEspecie);
	}

	@ApiOperation(value = "Busca todos registros")
	@GetMapping
	public Optional<List<CriacaoEspecie>> getAll() {
		return especieService.findAll();
	}

	@ApiOperation(value = "Busca por id")
	@GetMapping(path = "/id")
	public Optional<CriacaoEspecie> getById(@RequestParam @Validated Long id) {
		return especieService.findById(id);
	}

	@ApiOperation(value = "Busca por decricao")
	@GetMapping(path = "/descricao")
	public Optional<CriacaoEspecie> getById(@RequestParam @Validated String descricao) {
		return especieService.findByDescricao(descricao);
	}

	@ApiOperation(value = "Exclusao por id")
	@DeleteMapping(path = "/id")
	public ResponseEntity<Boolean> deleteById(Long id) {
		return especieService.deleteById(id);
	}

}
