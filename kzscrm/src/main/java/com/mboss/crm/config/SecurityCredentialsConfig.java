package com.mboss.crm.config;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.mboss.core.property.JwtConfiguration;
import com.mboss.security.config.SecurityTokenConfig;
import com.mboss.security.filter.JwtTokenAuthorizationFilter;
import com.mboss.security.token.converter.TokenConverter;

@EnableWebSecurity
public class SecurityCredentialsConfig extends SecurityTokenConfig {

	private final TokenConverter tokenConverter;

	public SecurityCredentialsConfig(
			JwtConfiguration jwtConfiguration,
			TokenConverter tokenConverter) {

		super(jwtConfiguration);
		this.tokenConverter = tokenConverter;
	}

	@Override
	protected void configure(HttpSecurity http) {

		try {
			http
					.addFilterAfter(new JwtTokenAuthorizationFilter(
							jwtConfiguration, tokenConverter),
							UsernamePasswordAuthenticationFilter.class);

			super.configure(http);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
