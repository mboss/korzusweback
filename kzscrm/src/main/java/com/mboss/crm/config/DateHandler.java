package com.mboss.crm.config;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class DateHandler extends StdDeserializer<Date> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DateHandler() {
		this(null);
	}

	public DateHandler(Class<?> clazz) {
		super(clazz);
	}

	@Override
	public Date deserialize(JsonParser jsonparser, DeserializationContext context) throws IOException, JsonProcessingException {

		String date = jsonparser.getText();
		SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss", Locale.ENGLISH);

		try {
			Date ddate = formatter.parse(date);
			return ddate;

		} catch (Exception e) {

			e.printStackTrace();
			return null;

		}
	}

}
