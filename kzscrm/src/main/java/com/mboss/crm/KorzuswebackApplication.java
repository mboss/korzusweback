package com.mboss.crm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.mboss.core.property.JwtConfiguration;

@SpringBootApplication
@EnableConfigurationProperties(value = JwtConfiguration.class)
@EntityScan({ "com.mboss.core.entity" })
@EnableJpaRepositories({ "com.mboss.core.repository" })
@ComponentScan({ "com.mboss" })
public class KorzuswebackApplication {

	public static void main(String[] args) {
		SpringApplication.run(KorzuswebackApplication.class, args);

		// Flyway flyway = Flyway.configure()
		// .dataSource("jdbc:postgresql://localhost:5433/kzsweb", "Luciano", "1425613")
		// .load();
		// flyway.migrate();

	}

}
