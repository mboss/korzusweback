INSERT INTO application_user (nome, role, senha) 
	SELECT 'admin','ADMIN', '$2a$10$OjcOVwsC1MMrrvtbSbn7xutdvJc2wkjA4/HL0SxjVOonXMNhdNnXi' 
	 WHERE NOT EXISTS (SELECT nome FROM application_user WHERE nome = 'admin');
	 
INSERT INTO application_user (nome, role, senha) 
	SELECT 'usuario','USER', '$2a$10$OjcOVwsC1MMrrvtbSbn7xutdvJc2wkjA4/HL0SxjVOonXMNhdNnXi' 
	 WHERE NOT EXISTS (SELECT nome FROM application_user WHERE nome = 'usuario');