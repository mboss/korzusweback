package com.mboss.gateway.security.config;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.mboss.core.property.JwtConfiguration;
import com.mboss.gateway.security.filter.GatewayJWTTokenAuthorizationFilter;
import com.mboss.security.config.SecurityTokenConfig;
import com.mboss.security.token.converter.TokenConverter;

@EnableWebSecurity
public class SecurityConfig extends SecurityTokenConfig {

	private final TokenConverter tokenConverter;

	public SecurityConfig(JwtConfiguration jwtConfiguration, TokenConverter tokenConverter) {
		super(jwtConfiguration);
		this.tokenConverter = tokenConverter;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.addFilterAfter(
				new GatewayJWTTokenAuthorizationFilter(jwtConfiguration, tokenConverter),
				UsernamePasswordAuthenticationFilter.class);

		super.configure(http);
	}

}
