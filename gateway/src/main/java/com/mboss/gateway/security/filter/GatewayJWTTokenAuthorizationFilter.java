
package com.mboss.gateway.security.filter;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.lang.NonNull;

import com.mboss.core.property.JwtConfiguration;
import com.mboss.security.filter.JwtTokenAuthorizationFilter;
import com.mboss.security.token.converter.TokenConverter;
import com.mboss.security.utils.SecurityContextUtil;
import com.netflix.zuul.context.RequestContext;
import com.nimbusds.jwt.SignedJWT;

public class GatewayJWTTokenAuthorizationFilter extends JwtTokenAuthorizationFilter {

	public GatewayJWTTokenAuthorizationFilter(JwtConfiguration jwtConfiguration, TokenConverter tokenConverter) {
		super(jwtConfiguration, tokenConverter);

	}

	@Override
	protected void doFilterInternal(@NonNull HttpServletRequest request, @NonNull HttpServletResponse response, @NonNull FilterChain chain) throws ServletException, IOException {
		String header = request.getHeader(jwtConfiguration.getHeader().getName());

		if (header == null || !header.startsWith(jwtConfiguration.getHeader().getPrefix())) {
			chain.doFilter(request, response);
			return;
		}

		String token = header.replace(jwtConfiguration.getHeader().getPrefix(), "").trim();

		String signedToken = tokenConverter.decryptToken(token);

		tokenConverter.validateTokenSignature(signedToken);

		try {
			SecurityContextUtil.setSecurityContext(SignedJWT.parse(signedToken));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		if (jwtConfiguration.getType().equalsIgnoreCase("signed"))
			RequestContext.getCurrentContext()
					.addZuulRequestHeader("Authorization",
							jwtConfiguration.getHeader().getPrefix() + signedToken);

		chain.doFilter(request, response);
	}

}
